﻿// Seusing System.Net.Sockets;
using System;
using System.Net.Sockets;
using System.Text;

namespace ControlClient
{

    class Client
    {
        static void Main()
        {
            StartClient();
        }

        static void StartClient()
        {
            string serverIp = "127.0.0.1";
            int serverPort = 12345;

            Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                clientSocket.Connect(serverIp, serverPort);

                Console.WriteLine("Connected to server at {0}:{1}", serverIp, serverPort);

                while (true)
                {
                    Console.WriteLine("Enter a number to send to the server or enter -1 to quit: ");
                    int? input = parseInt(Console.ReadLine());
                    if (input == -1) {
                        break;
                    }
                    if(input == null)
                    {
                        Console.WriteLine("Invalid data entered.");
                        continue;
                    }

                    byte[] sendData = BitConverter.GetBytes(input ?? 0);
                    clientSocket.Send(sendData);
                    Console.WriteLine("Send data {0}", input);

                    byte[] receiveBuffer = new byte[1024];
                    int bytesRead = clientSocket.Receive(receiveBuffer);

                    if (bytesRead > 0)
                    {
                        string serverResponse = Encoding.ASCII.GetString(receiveBuffer, 0, bytesRead);
                        Console.WriteLine("Server Response: " + serverResponse);
                    }
                    else
                    {
                        Console.WriteLine("Invalid response received from the server.");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally { clientSocket.Close(); }
        }

        static int? parseInt(string str) 
        { 
            try
            {
                return Int32.Parse(str);
            } catch (Exception ex)
            {
                return null;
            }
        }
    }
}