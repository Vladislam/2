﻿using ControlServer.Models;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    class ServerController
    {
        static void Main()
        {
            StartServer();
        }

        static void StartServer()
        {
            string ipAddress = "127.0.0.1";
            int port = 12345;

            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);

            Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                serverSocket.Bind(endPoint);
                serverSocket.Listen(20);

                Console.WriteLine("Server is listening on {0}:{1}", ipAddress, port);
                Socket clientSocket = serverSocket.Accept();

                while (true)
                {
                    if (clientSocket.Connected)
                    {
                        HandleClient(clientSocket);
                    }
                    else
                    {
                        Console.WriteLine("Client has closed the connection");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                serverSocket.Close();
            }
        }

        static void HandleClient(Socket clientSocket)
        {
            try { 
                byte[] buffer = new byte[4];
                int bytesRead = clientSocket.Receive(buffer);

                if (bytesRead == 4)
                {
                    int clientId = BitConverter.ToInt32(buffer, 0);
                    Console.WriteLine("Received from client: " + clientId);

                    using (var dbContext = new my_dbContext())
                    {
                        var user = dbContext.Users.FirstOrDefault(u => u.Id == clientId);

                        string response;
                        if (user != null)
                        {
                            response = user.Info;
                        }
                        else
                        {
                            response = "Record was not found";
                        }

                        byte[] responseData = Encoding.ASCII.GetBytes(response);
                        clientSocket.Send(responseData);
                    }
                }
                else
                {
                    Console.WriteLine("Invalid data from client.");
                    clientSocket.Send(Encoding.ASCII.GetBytes("Invalid data"));
                }
            }
            catch (Exception ex)
            {
                if (!clientSocket.Connected) { return; }
                Console.WriteLine("Error handling client: " + ex.Message);
            }
        }
    }
}
